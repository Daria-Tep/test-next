import styles from "../../styles/Home.module.css";
import Link from "next/link";


export default function NavBar() {
    return (
        <nav className={styles.nav}>
            <Link href='/'><a>Home</a></Link>
            <Link href='/about'><a>About</a></Link>
            <Link href='/catalog'><a>Catalog</a></Link>
            <Link href='/contacts'><a>Contacts</a></Link>
        </nav>
    )
}