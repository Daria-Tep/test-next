import styles from '../../styles/User.module.css'


export default function User(props) {
    return (
        <div className={styles.user}>
            <div>Name: {props.name} </div>
            <div>Email: {props.email}</div>
        </div>
    )
}