import Link from "next/link";
import styles from "../styles/Contacts.module.css"


export default function Contacts() {
    return (
        <div className={styles.contacts}>
            <h2>Contacts page</h2>
            <Link href='/'><a>back</a></Link>
        </div>
    )
}