import Link from "next/link";
import styles from "../styles/About.module.css"
import Image from "next/image";


export async function getStaticProps() {
    const data = await fetch('https://jsonplaceholder.typicode.com/photos');
    const photos = await data.json();

    return {
        props: { photos },
    }
}

function about({ photos }) {
    return (
        <div className={styles.about}>
            <h2>About page</h2>
            <p>Local image:</p>
            <Image src={'/mem1.jpg'}
                   width={200}
                   height={200}
                   alt={'img'} />

        <div>
            <p>Image from the server, but without the embedded Image tag: </p>
            {photos && photos.slice(0,1).map( photo => (
                <img src={photo.url} alt={'alt'} width={200} height={200}/>

            ) )}
        </div>

            <div>
                <Link href='/'><a>back</a></Link>
            </div>

        </div>
    )
}



export default about;