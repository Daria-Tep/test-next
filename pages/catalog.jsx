import Link from "next/link";
import styles from "../styles/Catalog.module.css"

export async function getServerSideProps() {
    const res = await fetch(`https://jsonplaceholder.typicode.com/albums`)
    const data = await res.json()

    // Pass data to the page via props
    return { props: { data } }
}

export default function Catalog({ data }) {

    return (
        <div className={styles.catalog}>
            <h2>Catalog page</h2>
            <p>used getServerSideProps:</p>
            <div>
                {data && data.slice(0,10).map( album => (
                    <div>Title: {album.title}</div>
                ) )}
            </div>

            <div>
            <Link href='/'><a>back</a></Link>
            </div>
        </div>
    )
}


