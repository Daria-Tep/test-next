import Head from 'next/head'
import {useEffect, useState} from "react";
import styles from '../styles/Home.module.css'
import Layout from "./components/Layout";
import MyApp from "./_app";
import User from "./components/User";


export async function getStaticProps() {
    const data = await fetch('https://jsonplaceholder.typicode.com/users');
    const users = await data.json();

    return {
        props: { users },
    }
}

export default function Home({ users }) {



  return (
    <div className={styles.container}>
        <Head></Head>

        <h1>Home page</h1>

        <p>used getStaticProps:</p>
        {users && users.map(user => (
            <User name={user.name} email={user.email} />
            )
        )}

    </div>
  )
}



